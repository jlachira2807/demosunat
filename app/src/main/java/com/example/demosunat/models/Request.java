package com.example.demosunat.models;

import com.example.demosunat.utils.Converts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Request {
private String reqId;
private String reqEmi;
private Date reqDate;
private String reqType;
private String reqDescription;

    public Request(String reqId, String reqEmi, Date reqDate, String reqType) {
        this.reqId = reqId;
        this.reqEmi = reqEmi;
        this.reqDate = reqDate;
        this.reqType = reqType;
    }

    public Request() {
    }

    public String getReqId() {
        return reqId;
    }

    public Request setReqId(String reqId) {
        this.reqId = reqId;
        return this;
    }

    public String getReqEmi() {
        return reqEmi;
    }

    public Request setReqEmi(String reqEmi) {
        this.reqEmi = reqEmi;
        return this;
    }

    public Date getReqDate() {
        return reqDate;
    }

    public Request setReqDate(Date reqDate) {
        this.reqDate = reqDate;
        return this;
    }

    public String getReqType() {
        return reqType;
    }

    public Request setReqType(String reqType) {
        this.reqType = reqType;
        return this;
    }

    public String getReqDescription() {
        return reqDescription;
    }

    public Request setReqDescription(String reqDescription) {
        this.reqDescription = reqDescription;
        return this;
    }

public  static class Builder{

private Request request;
private List<Request> requests;

    public Builder() {
    this.request = new Request();
    }

    public Builder(Request request) {
        this.request = request;
    }

    public Builder(List<Request> requests) {
        this.requests = requests;
    }

    public Request build(){
        return  request;
    }

    public List<Request> buildAll(){
        return requests;
    }

    public static Request.Builder from(JSONObject jsonRequest) {
        try {
            return new Builder(new Request(
                    jsonRequest.getString("code-req"),
                    jsonRequest.getString("emisor"),
                    Converts.ConvertStringToDate(jsonRequest.getString("date")),
                    jsonRequest.getString("type")

            ));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Builder from(JSONArray jsonRequests) {
        int length = jsonRequests.length();
        List<Request> requests = new ArrayList<>();
        for(int i = 0; i < length; i++) {
            try {
                requests.add(Builder.from(jsonRequests.getJSONObject(i)).build());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return new Builder(requests);
    }


}


}
