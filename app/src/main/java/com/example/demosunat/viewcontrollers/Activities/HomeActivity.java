package com.example.demosunat.viewcontrollers.Activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demosunat.R;
import com.example.demosunat.models.Request;
import com.example.demosunat.utils.BottomNavigationBehavior;
import com.example.demosunat.viewcontrollers.Adapters.Requests2Adapter;
import com.example.demosunat.viewcontrollers.Adapters.RequestsAdapter;
import com.example.demosunat.viewcontrollers.Fragments.Requests2Fragment;
import com.example.demosunat.viewcontrollers.Fragments.RequestsFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity  {

    private TextView mTextMessage;
    private RequestsAdapter mAdapter;
    private Requests2Adapter mAdapter2;
    private SearchView searchView;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            return navigateTo(item);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // toolbar fancy stuff
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Solicitudes");

        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigateTo(navigation.getMenu().findItem(R.id.navigation_home));



        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());
    }
    public void initializeRequest(RequestsAdapter requestsAdapter){
        mAdapter = requestsAdapter;
    }
    public void initializeRequest2(Requests2Adapter requestsAdapter){
        mAdapter2 = requestsAdapter;
    }


    private Fragment getFragmentFor (MenuItem item){




        switch (item.getItemId()) {
            case R.id.navigation_home:
                return new RequestsFragment();
            case R.id.navigation_dashboard:
                return new Requests2Fragment();
            case R.id.navigation_notifications:
                return new RequestsFragment();
            default:
                return new RequestsFragment();
        }
    }

    private boolean navigateTo(MenuItem item){
        item.setChecked(true);
        return getSupportFragmentManager().beginTransaction().replace(R.id.content,getFragmentFor(item)).commit()>0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mAdapter.getFilter().filter(query);
                if(mAdapter2!=null) {
                    mAdapter2.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
               mAdapter.getFilter().filter(query);
                if(mAdapter2!=null) {
                    mAdapter2.getFilter().filter(query);
                }
                return false;
            }
        });
        return true;
    }

}
