package com.example.demosunat.viewcontrollers.Fragments;


import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;

import com.example.demosunat.R;
import com.example.demosunat.models.Request;
import com.example.demosunat.viewcontrollers.Activities.HomeActivity;
import com.example.demosunat.viewcontrollers.Adapters.RequestsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestsFragment extends Fragment {
    private List<Request> requests;


    private RecyclerView requestsRecyclerView;
    private RecyclerView.LayoutManager requestsLayoutManager;
    private RequestsAdapter requestsAdapter;

    public RequestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_requests, container, false);
        requests = new ArrayList<>();

        requestsRecyclerView = view.findViewById(R.id.recycler_request_pending);
        requestsAdapter = new RequestsAdapter(requests);
        requestsLayoutManager = new GridLayoutManager(view.getContext(), 1);
        requestsRecyclerView.setAdapter(requestsAdapter);
        requestsRecyclerView.setLayoutManager(requestsLayoutManager);
        whiteNotificationBar(requestsRecyclerView);

        loadJSONFromAsset();
        ((HomeActivity) getActivity()).initializeRequest(requestsAdapter);
        return view;
    }

    public ArrayList<Request> loadJSONFromAsset() {
        ArrayList<Request> reqList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getContext().getResources().getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("solicitudes");
 /*
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                MyLocations location = new MyLocations();
                location.setLat((float) jo_inside.getDouble("lat"));
                location.setLng((float) jo_inside.getDouble("lng"));
                location.setKey(jo_inside.getString("key"));
                location.setRadius(jo_inside.getInt("radius"));
                location.setName(jo_inside.getString("name"));
                location.setAudio_file(jo_inside.getString("audio_file"));

                locList.add(location);


            }
    */
            requests = Request.Builder.from(m_jArry).buildAll();
            requestsAdapter.setRequests(requests);
            requestsAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return reqList;
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.WHITE);
        }
    }


}