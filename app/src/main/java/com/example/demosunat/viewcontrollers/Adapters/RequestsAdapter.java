package com.example.demosunat.viewcontrollers.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.demosunat.R;
import com.example.demosunat.models.Request;

import java.util.ArrayList;
import java.util.List;

public class RequestsAdapter extends  RecyclerView.Adapter<RequestsAdapter.ViewHolder> implements Filterable {
    private List<Request> requests;
    private Context context;
    private List<Request> requestsListFiltered;
    public RequestsAdapter(List<Request> requests) {
        this.requests = requests;
    }




    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_request, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.updateView(requests.get(i));
    }

    @Override
    public int getItemCount() {
        return requests.size();
    }

    public List<Request> getRequests() {
        return requests;
    }

    public RequestsAdapter setRequests(List<Request> requests) {
        this.requests = requests;
        this.requestsListFiltered = requests;
        return this;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView emiName,reqDate,reqCode,reqType,reqSubject;
        private Button btnAceppt,btnReject;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            emiName=itemView.findViewById(R.id.textView_request_emisor);
            reqDate=itemView.findViewById(R.id.textView_request_date);
            reqCode=itemView.findViewById(R.id.textView_request_code);
            reqType=itemView.findViewById(R.id.textView_request_type);
            btnAceppt=itemView.findViewById(R.id.button_request_accept);
            btnReject = itemView.findViewById(R.id.button_request_reject);
        }

        public void updateView(final Request request){
            emiName.setText(request.getReqEmi());
            reqDate.setText(request.getReqDate().toString());
            reqCode.setText(request.getReqId());
            reqType.setText(request.getReqType());
            btnAceppt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(getAdapterPosition());
                }
            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(getAdapterPosition());
                }
            });
        }
    }

    public void removeItem(int position){

        requests.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    requests = requestsListFiltered;
                } else {
                    List<Request> filteredList = new ArrayList<>();
                    for (Request row : requestsListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getReqEmi().toLowerCase().contains(charString.toLowerCase()) || row.getReqType().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    requests = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = requests;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                requests = (ArrayList<Request>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
