package com.example.demosunat.viewcontrollers.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.demosunat.R;
import com.example.demosunat.models.Request;

import java.util.ArrayList;
import java.util.List;

public class Requests2Adapter extends  RecyclerView.Adapter<Requests2Adapter.ViewHolder> implements Filterable {
    private List<Request> requests;
    private Context context;
    private List<Request> requestsListFiltered;

    public Requests2Adapter(List<Request> requests) {
        this.requests = requests;
    }


    @NonNull
    @Override
    public Requests2Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_request2, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Requests2Adapter.ViewHolder viewHolder, int i) {
        viewHolder.updateView(requests.get(i));
    }

    @Override
    public int getItemCount() {
        return requests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView emiName,reqDate,reqCode,reqType,reqSubject;
        public RelativeLayout viewBackground, viewForeground;
        private Button btnAccept;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            emiName=itemView.findViewById(R.id.textView_request_emisor2);
            reqDate=itemView.findViewById(R.id.textView_request_date2);
            reqCode=itemView.findViewById(R.id.textView_request_code2);
            reqType=itemView.findViewById(R.id.textView_request_type2);
            btnAccept = itemView.findViewById(R.id.button_request_accept2);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }

        public void updateView(final Request request){
            emiName.setText(request.getReqEmi());
            reqDate.setText(request.getReqDate().toString());
            reqCode.setText(request.getReqId());
            reqType.setText(request.getReqType());
            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(getAdapterPosition());
                }
            });
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    requests = requestsListFiltered;
                } else {
                    List<Request> filteredList = new ArrayList<>();
                    for (Request row : requestsListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getReqEmi().toLowerCase().contains(charString.toLowerCase()) || row.getReqType().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    requests = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = requests;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                requests = (ArrayList<Request>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public Requests2Adapter setRequests(List<Request> requests) {
        this.requests = requests;
        this.requestsListFiltered = requests;
        return this;
    }

    public void removeItem(int position) {
        requests.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(Request item, int position) {
        requests.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }


}