package com.example.demosunat.viewcontrollers.Fragments;


import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.demosunat.R;
import com.example.demosunat.models.Request;
import com.example.demosunat.utils.RecyclerItemTouchHelper;
import com.example.demosunat.viewcontrollers.Activities.HomeActivity;
import com.example.demosunat.viewcontrollers.Adapters.Requests2Adapter;
import com.example.demosunat.viewcontrollers.Adapters.RequestsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Requests2Fragment extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    private List<Request> requests;


    private RecyclerView requestsRecyclerView;
    private RecyclerView.LayoutManager requestsLayoutManager;
    private Requests2Adapter requestsAdapter;
    private CoordinatorLayout coordinatorLayout;
    public Requests2Fragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_requests2, container, false);
        requests = new ArrayList<>();
        coordinatorLayout = view.findViewById(R.id.coordinator_request2);
        requestsRecyclerView = view.findViewById(R.id.recycler_view);
        requestsAdapter = new Requests2Adapter(requests);
        requestsLayoutManager = new GridLayoutManager(view.getContext(), 1);
        requestsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        requestsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        requestsRecyclerView.setAdapter(requestsAdapter);
        requestsRecyclerView.setLayoutManager(requestsLayoutManager);
        whiteNotificationBar(requestsRecyclerView);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT,this );
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(requestsRecyclerView);

        loadJSONFromAsset();
        ((HomeActivity) getActivity()).initializeRequest2(requestsAdapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT ) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                // Row is swiped from recycler view
                // remove it from adapter
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        // attaching the touch helper to recycler view
        new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(requestsRecyclerView);

        return view;
    }

    public ArrayList<Request> loadJSONFromAsset() {
        ArrayList<Request> reqList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getContext().getResources().getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("solicitudes");
 /*
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                MyLocations location = new MyLocations();
                location.setLat((float) jo_inside.getDouble("lat"));
                location.setLng((float) jo_inside.getDouble("lng"));
                location.setKey(jo_inside.getString("key"));
                location.setRadius(jo_inside.getInt("radius"));
                location.setName(jo_inside.getString("name"));
                location.setAudio_file(jo_inside.getString("audio_file"));

                locList.add(location);


            }
    */
            requests = Request.Builder.from(m_jArry).buildAll();
            requestsAdapter.setRequests(requests);
            requestsAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return reqList;
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.WHITE);
        }
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof Requests2Adapter.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = requests.get(viewHolder.getAdapterPosition()).getReqId();

            // backup of removed item for undo purpose
            final Request deletedItem = requests.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            requestsAdapter.removeItem(viewHolder.getAdapterPosition());

            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + " ha sido rechazado!", Snackbar.LENGTH_LONG);
            snackbar.setAction("DESHACER", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    requestsAdapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();

        }
    }


}
